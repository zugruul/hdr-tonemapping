#include <math.h>
#include <string.h>		// para usar strings

// Rotinas para acesso da OpenGL
#include "opengl.h"

// Rotinas para leitura de arquivos .hdr
#include "rgbe.h"

#define min(a,b) ((a) < (b) ? a : b)

// Variáveis globais a serem utilizadas:

// Dimensões da imagem de entrada
int sizeX, sizeY;

// Imagem de entrada
RGBf* image;

// Imagem de saída
RGB8* image8;

// Fator de exposição inicial
float exposure = 1.0;
float scaling = 1.0;
float gamma = 1.8;

// Modo de exibição atual
int modo;

// Protótipos
float scaleColor(float color, float scaling);
float gammaCorrection(float color, float correction);


// Função pow mais eficiente (cerca de 7x mais rápida)
float fastpow(float a, float b);
float fastpow(float a, float b) {
     union { float f; int i; }
      u = { a };
      u.i = (int)(b * (u.i - 1065307417) + 1065307417);
      return u.f;
}

// Função principal de processamento: ela deve chamar outras funções
// quando for necessário (ex: algoritmos de tone mapping, etc)
void process()
{
    printf("Exposure: %.3f\n", exposure);
    printf("Gamma: %.3f\n", gamma);
    printf("Scaling: %.3f\n", scaling);
    //
    // SUBSTITUA este código pelos algoritmos a serem implementados
    //
    int pos;
    for(pos=0; pos < sizeX * sizeY; pos++) {
        float exposedR = image[pos].r * exposure;
        float exposedG = image[pos].g * exposure;
        float exposedB = image[pos].b * exposure;
        float tonemappedR, tonemappedG, tonemappedB;

        if(modo == SCALE)
        {
            tonemappedR = scaleColor(exposedR, scaling);
            tonemappedG = scaleColor(exposedG, scaling);
            tonemappedB = scaleColor(exposedB, scaling);
        } else {
            tonemappedR = gammaCorrection(exposedR, gamma);
            tonemappedG = gammaCorrection(exposedG, gamma);
            tonemappedB = gammaCorrection(exposedB, gamma);
        }

        image8[pos].r = (unsigned char) (min(1.0, tonemappedR) * 255);
        image8[pos].g = (unsigned char) (min(1.0, tonemappedG) * 255);
        image8[pos].b = (unsigned char) (min(1.0, tonemappedB) * 255);
    }

    //
    // NÃO ALTERAR A PARTIR DAQUI!!!!
    //
    buildTex();
}

float scaleColor(float color, float scaling) {
    return (float) color / (color + scaling);
}

float gammaCorrection(float color, float correction) {
    float pot = 1 / correction;
    return fastpow(color, pot);
}

int main(int argc, char** argv)
{
    if(argc==1) {
        printf("hdrvis [image file.hdr]\n");
        exit(1);
    }

    // Inicialização da janela gráfica
    init(argc,argv);

    // Lê imagem
    FILE* arq = fopen(argv[1],"rb");
    // Carrega na memória o arquivo e salva nos endereços de width (sizeX) e height (sizeY) a altura e largura
    RGBE_ReadHeader(arq, &sizeX, &sizeY, NULL);
    // Aloca imagem float
    image = (RGBf*) malloc(sizeof(RGBf) * sizeX * sizeY);
    // Aloca memória para imagem de 24 bits
    image8 = (RGB8*) malloc(sizeof(RGB8) * sizeX * sizeY);

    // Leitura da imagem para a memória
    int result = RGBE_ReadPixels_RLE(arq, (float*)image, sizeX, sizeY);
    if (result == RGBE_RETURN_FAILURE) {
        printf("Error - for now not treated");
    }
    fclose(arq);

    printf("%d x %d\n", sizeX, sizeY);

    exposure = 1.0f; // exposição inicial
    scaling = 0.3f; // scaling de tonemapping por escala
    gamma = 1.8f;
    // Aplica processamento inicial
    process();

    // Não retorna... a partir daqui, interação via teclado e mouse apenas, na janela gráfica
    glutMainLoop();
    return 0;
}

